Uses AWS Lambda to create a directory listing page of an S3 bucket without having to call the AWS API from the client.

When a new object is added. A Lambda event is fired which updates the directory map.

*TODO:* Need to support updating when files are removed.

 - [View Website](http://s3.awesomebox.net)