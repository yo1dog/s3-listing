/**
 * @module S3ListingTemplateHelper
 * 
 * Provides functions that may be useful when creating a {@link module:S3ListingTemplate}.
 */
(function() {
  "use strict";
  
  
  
  /**
   * @exports
   */
  var S3ListingTemplateHelper = {
  };
  
  window.S3ListingTemplateHelper = S3ListingTemplateHelper;
  
  
  
  /**
   * Escapes HTML reserved characters for safe output of a string into an HTML environment.
   * 
   * @param {string} str - String to escape.
   * 
   * @returns {string} Escaped string.
   */
  S3ListingTemplateHelper.htmlEscape = function(str) {
    return str
      .replace(/&/g, "&amp;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#39;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
  };
})();