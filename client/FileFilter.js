/**
 * @module FileFilter
 * 
 * Provides the {@link FileFilter} class.
 */
(function() {
  "use strict";
  
  
  
  /**
   * @enum {string}
   */
  var MatchAgainstEnum = Object.freeze({
    name    : "name",
    filepath: "filepath"
  });
  
  /**
   * @enum {string}
   */
  var AppliesToEnum = Object.freeze({
    files: "files",
    dirs : "dirs",
    both : "both"
  });
  
  
  
  /**
   * @exports
   * @class
   * @classdesc Defines a matching pattern for files.
   * 
   * @param {RegExp|string}    matching     - Regex or string to use for matching. If a regex
   *                                          is given, {@link RegExp#test} will be used to
   *                                          test the subject. If a string is given, the
   *                                          subject must equal (===) the string for a
   *                                          positive test.
   * @param {MatchAgainstEnum} matchAgainst - What should be used as the subject of the test.
   * @param {AppliesToEnum}    appliesTo    - What kinds of files this should apply to.
   */
  function FileFilter(matching, matchAgainst, appliesTo) {
    matchAgainst = matchAgainst || MatchAgainstEnum.name;
    appliesTo    = appliesTo    || AppliesToEnum.both;
    
    if (!matching) {
      throw new Error("Invalid FileFilter. No matching given.");
    }
    
    if (!MatchAgainstEnum[matchAgainst]) {
      throw new Error("Invalid FileFilter. Invalid matchAgainst value \"" + matchAgainst + "\", use MatchAgainstEnum.");
    }
    
    if (!AppliesToEnum[appliesTo]) {
      throw new Error("Invalid FileFilter. Invalid appliesTo value \"" + appliesTo + "\", use AppliesToEnum.");
    }
    
    this.matching     = matching;
    this.matchAgainst = matchAgainst;
    this.appliesTo    = appliesTo;
  }
  
  window.FileFilter = FileFilter;
  
  
  
  /**
   * Tests a file.
   * 
   * @param {string}  filepath - Filepath of the file.
   * @param {string}  filename - Filename of the file.
   * @param {boolean} isDir    - If the file is a directory or not.
   * 
   * @returns {number} If the file should go through the filter (true) or not (false).
   */
  FileFilter.prototype.testFile = function(filepath, filename, isDir) {
    // check if this filter should apply
    if (isDir) {
      if (this.appliesTo === AppliesToEnum.files) {
        return false;
      }
    }
    else {
      if (this.appliesTo === AppliesToEnum.dirs) {
        return false;
      }
    }
    
    // get the subject for the test
    var subject = this.matchAgainst === MatchAgainstEnum.filepath? filepath : filename;
    if (subject.charAt(subject.length - 1) === "/") {
      subject = subject.substring(0, subject.length - 1);
    }
    
    // test the subject
    if (this.matching instanceof RegExp) {
      return this.matching.test(subject);
    }
    else {
      return this.matching === subject;
    }
  };
  
  
  
  FileFilter.MatchAgainstEnum = MatchAgainstEnum;
  FileFilter.AppliesToEnum    = AppliesToEnum;
})();