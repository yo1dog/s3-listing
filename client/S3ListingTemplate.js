/**
 * @module S3ListingTemplate
 * 
 * Provides the {@link S3ListingTemplate} class.
 */
(function() {
  "use strict";
  
  
  
  /**
   * @name CreateHeaderHTMLFunction
   * @function
   * @param {module:S3ListingMap} s3ListingMap - The S3 Listing Map.
   * @param {string} dirFilepath               - Filepath to directory being displayed.
   * 
   * @returns {string} HTML
   */
  
  /**
   * @name CreateFileHTMLFunction
   * @function
   * @param {module:S3ListingMap}           s3ListingMap - The S3 Listing Map.
   * @param {string}                        dirFilepath  - Filepath to directory being displayed.
   * @param {string}                        filename     - Filename of the current file.
   * @param {module:S3ListingMap.FileEntry} fileEntry    - File entry for the current file.
   * @param {string}                        fileURL      - URL for the current file.
   * @param {boolean}                       isHidden    - If the current file should be hidden or not.
   * 
   * @returns {string} HTML
   */
  
  /**
   * @name CreateFooterHTMLFunction
   * @function
   * @param {module:S3ListingMap} s3ListingMap - The S3 Listing Map.
   * @param {string}              dirFilepath - Filepath to directory being displayed.
   * 
   * @returns {string} HTML
   */
  
  
  /**
   * @exports
   * @class
   * @classdesc Defines functions for creating HTML using the {@link module:S3ListingViewer}.
   * 
   * @param {CreateHeaderHTMLFunction} createHeaderHTMLFunction - Called first.
   * @param {CreateFileHTMLFunction}   createFileHTMLFunction   - Called for each file to be displayed.
   * @param {CreateFooterHTMLFunction} createFooterHTMLFunction - Called last.
   */
  function S3ListingTemplate(createHeaderHTMLFunction, createFileHTMLFunction, createFooterHTMLFunction) {
    this.createHeaderHTMLFunction = createHeaderHTMLFunction;
    this.createFileHTMLFunction   = createFileHTMLFunction;
    this.createFooterHTMLFunction = createFooterHTMLFunction;
  }
  
  window.S3ListingTemplate = S3ListingTemplate;
})();