/**
 * @module S3ListingViewer
 * 
 * Provides the {@link S3ListingViewer} class.
 */
(function() {
  "use strict";
  
  
  
  /**
   * @exports
   * @class
   * @classdesc A utility for displaying an S3 Listing as HTML.
   * 
   * @requires module:S3ListingMap
   * @requires module:S3ListingTemplate
   * @requires module:FileFilter
   * 
   * @param {module:S3ListingMap}      s3ListingMap      - Listing map to use for displaying.
   * @param {module:S3ListingTemplate} s3ListingTemplate - Template to use for displaying.
   * @param {module:FileFilter[]}      [hideFilters=[]]  - All files that match these filters will be hidden.
   */
  function S3ListingViewer(s3ListingMap, s3ListingTemplate, hideFilters) {
    this.map         = s3ListingMap;
    this.template    = s3ListingTemplate;
    this.hideFilters = hideFilters || [];
  }
  
  window.S3ListingViewer = S3ListingViewer;
  
  
  
  /**
   * Creates the HTML for the listing.
   * 
   * @returns {string|null} HTML or null if the filepath given in the query string does not exist.
   */
  S3ListingViewer.prototype.createHTML = function() {
    var dirFilepath = getFilepathFromQueryString();
    
    // get directory entry
    var dirEntry = this.map.directories[dirFilepath];
    if (!dirEntry) {
      return null;
    }
    
    
    // get filenames from directory entry
    var filenames = [];
    
    for (var filename in dirEntry.files) {
      filenames.push(filename);
    }
    
    // sort filenames alphabetically
    filenames.sort(function(a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    
    
    // create HTML
    var listHTML = this.template.createHeaderHTMLFunction(this.map, dirFilepath);
    
    for (var i = 0; i < filenames.length; ++i) {
      var filename  = filenames[i];
      var filepath  = dirFilepath + filename;
      var fileEntry = dirEntry.files[filename];
      
      
      // check if hidden
      var isHidden = false;
      for (var j = 0; j < this.hideFilters.length; ++j) {
        if (this.hideFilters[j].testFile(filepath, filename, fileEntry.isDir)) {
          isHidden = true;
          break;
        }
      }
      
      
      // create URL
      var fileURL = getURLForFile(filepath, fileEntry.isDir);
      
      
      listHTML += this.template.createFileHTMLFunction(this.map, dirFilepath, filename, fileURL, isHidden);
    }
    
    listHTML += this.template.createFooterHTMLFunction(this.map, dirFilepath);
    return listHTML;
  };
  
  
  
  /**
   * Returns a URL for the given file.
   * 
   * @params {string}  filepath - Filepath of file.
   * @params {boolean} isDir    - If the file is a directory (true) or not (false).
   */
  function getURLForFile(filepath, isDir) {
    if (isDir) {
      var pathQueryStringValue = filepath;
      if (pathQueryStringValue.length === 0 || pathQueryStringValue.charAt(pathQueryStringValue.length - 1) !== "/") {
        pathQueryStringValue += "/";
      }
      
      return "?p=" + encodePathQueryStringValue(pathQueryStringValue);
    }
    else {
      var fileURL = filepath;
      
      if (fileURL.length > 1 && fileURL.charAt(0) === "/") {
        fileURL = fileURL.substring(1);
      }
      
      return fileURL;
    }
  }
  S3ListingViewer.getURLForFile = getURLForFile;
  
  
  
  /**
   * Gets the current filepath from the query string.
   * 
   * @returns Filepath.
   */
  function getFilepathFromQueryString() {
    if (location.search.indexOf("?p=") !== 0) {
      return "/";
    }
    
    var path = decodeURIComponent(location.search.substring(3));
    
    if (path.length === 0) {
      return "/";
    }
    
    if (path.charAt(path.length - 1) !== '/') {
      path += "/";
    }
    
    return path;
  }
  
  
  /**
   * Encodes a filepath to be used in the query string.
   * 
   * @param {string} filepath - Filepath to encode.
   * 
   * @returns {string} Encoded filepath.
   */
  function encodePathQueryStringValue(filepath) {
    return encodeURIComponent(filepath).replace(/%2F/gi, "/").replace(/\+/g, "%2B");
  }
})();