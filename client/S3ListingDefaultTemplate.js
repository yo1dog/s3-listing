/**
 * @module S3ListingDefaultTemplate
 * 
 * Provides a default {@link module:S3ListingTemplate}.
 * 
 * @requires module:S3ListingTemplate
 * @requires module:S3ListingTemplateHelper
 * @requires module:S3ListingViewer
 */
(function() {
  "use strict";
  
  
  
  /**
   * @exports
   */
  var S3ListingDefaultTemplate = {
  };
  
  window.S3ListingDefaultTemplate = S3ListingDefaultTemplate;
  
  
  
  /**
   * Creates an instance of the default template.
   * 
   * @returns {module:S3ListingTemplate} Default tempalte.
   */
  S3ListingDefaultTemplate.create = function() {
    return new S3ListingTemplate(
        createHeaderHTMLFunction,
        createFileHTMLFunction,
        createFooterHTMLFunction
    );
  };
  
  
  
  /**
   * @type {module:S3ListingTemplate~CreateHeaderHTMLFunction}
   */
  function createHeaderHTMLFunction(s3ListingMap, dirFilepath) {
    var html =
      "<p><b>" + createClickablePathHTML(dirFilepath) + "</b></p>\n" + 
      "<table>\n" +
      "<thead>\n" +
      "<tr><th>Name</th><th>Last Modified</th><th>Size</th></tr>\n" +
      "</thead>\n" +
      "<tbody>\n";
    
    var parentDirPath = s3ListingMap.directories[dirFilepath].parentDirPath;
    if (parentDirPath) {
      html += createFileHTMLFunction(s3ListingMap, parentDirPath, "..", S3ListingViewer.getURLForFile(parentDirPath, true), false);
    }
    
    return html;
  }
  
  
  /**
   * Splits the given filepath into it individual clickable directories.
   * 
   * @param {string} dirFilepath - Filepath to split.
   * 
   * @returns {string} HTML;
   */
  function createClickablePathHTML(dirFilepath) {
    var html = "<a href=\"" + S3ListingTemplateHelper.htmlEscape(S3ListingViewer.getURLForFile("/", true)) + "\">[root]</a> / ";
    
    var parts = dirFilepath.split("/");
    var currentPath = "/";
    
    for (var i = 0; i < parts.length; ++i) {
      var part = parts[i];
      
      if (!part) {
        continue;
      }
      
      currentPath += part + "/";
      html += "<a href=\"" + S3ListingTemplateHelper.htmlEscape(S3ListingViewer.getURLForFile(currentPath, true)) + "\">" + S3ListingTemplateHelper.htmlEscape(part) + "</a> / ";
    }
    
    return html;
  }
  
  
  
  /**
   * @type {module:S3ListingTemplate~CreateFileHTMLFunction}
   */
  function createFileHTMLFunction(s3ListingMap, dirFilepath, filename, fileURL, isHidden) {
    if (isHidden) {
      return "";
    }
    
    var dirEntry  = s3ListingMap.directories[dirFilepath];
    var fileEntry = dirEntry.files[filename];
    
    
    var name;
    var lastModified;
    var size;
    
    if (fileEntry.isDir) {
      name = filename + "/";
    }
    else {
      name = filename;
      
      // last modified
      if (typeof fileEntry.lastModifiedTimestampMS !== "undefined") {
        var date = new Date(fileEntry.lastModifiedTimestampMS);
        
        // create the timezone string
        var timezoneOffsetMins  = date.getTimezoneOffset() * -1;
        var timezoneOffsetHours = Math.floor(timezoneOffsetMins / 60);
        
        // hours
        var timezoneStrHours = "" + Math.abs(timezoneOffsetHours);
        if (timezoneStrHours.length === 1) {
          timezoneStrHours = "0" + timezoneStrHours;
        }
        timezoneStrHours = (timezoneOffsetHours < 0? "-" : "+") + timezoneStrHours;
        
        // mins
        var timezoneStrMins = "" + (Math.abs(timezoneOffsetMins) % 60);
        if (timezoneStrMins.length === 1) {
          timezoneStrMins = "0" + timezoneStrMins;
        }
        
        var timeZoneStr = " " + timezoneStrHours + timezoneStrMins;
        
        lastModified = date.toLocaleString() + timeZoneStr;
      }
      
      // size
      if (typeof fileEntry.sizeInBytes !== "undefined") {
        var sizeInBytes = fileEntry.sizeInBytes;
        var KB = 1024;
        var MB = 1024 * 1024;
        var GB = 1024 * 1024 * 1024;
        
        if (sizeInBytes < KB) {
          size = sizeInBytes + "B";
        }
        else if (sizeInBytes < MB) {
          size = roundSize(sizeInBytes / KB) + "K";
        }
        else if (sizeInBytes < GB) {
          size = roundSize(sizeInBytes / MB) + "M";
        }
        else {
          size = roundSize(sizeInBytes / GB) + "G";
        }
      }
    }
    
    
    var hasLastModified = lastModified? true : false;
    var hasSize         = size        ? true : false;
    
    lastModified = hasLastModified? lastModified : "-";
    size         = hasSize        ? size         : "-";
    
    var nameClass         = "name";
    var lastModifiedClass = "lastModified" + (hasLastModified? "" : " empty");
    var sizeClass         = "size"         + (hasSize        ? "" : " empty");
      
    var html = 
      "<tr>\n" +
      "<td class=\"" + nameClass         + "\"><a href=\"" + S3ListingTemplateHelper.htmlEscape(fileURL) + "\">" + S3ListingTemplateHelper.htmlEscape(name) + "</a></td>\n" +
      "<td class=\"" + lastModifiedClass + "\">" + S3ListingTemplateHelper.htmlEscape(lastModified) + "</td>\n" +
      "<td class=\"" + sizeClass         + "\">" + S3ListingTemplateHelper.htmlEscape(size) + "</td>\n" +
      "</tr>\n";
    
    return html;
  }
  
  
  /**
   * Rounds the given size to 1 decimal point.
   */
  function roundSize(size) {
    return (Math.floor(size * 10) * 0.1).toFixed(1);
  }
  
  
  
  /**
   * @type {module:S3ListingTemplate~CreateFooterHTMLFunction}
   */
  function createFooterHTMLFunction(s3ListingMap, dirFilepath) {
    var html =
      "</tbody>\n" +
      "</table>\n";
    
    return html;
  }
})();