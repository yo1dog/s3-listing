/**
 * @module S3ListingMapLoader
 * 
 * Provides functions for fetching and loading an S3 Listing Map.
 * 
 * @requires module:Config
 * @requires module:S3ListingMap
 */
(function() {
  "use strict";
  
  
  
  /**
   * @exports
   */
  var S3ListingMapLoader = {
  };
  
  window.S3ListingMapLoader = S3ListingMapLoader;
  
  
  
  /**
   * Fetches and returns the S3 Listing Map for the given S3 bucket. It
   * does this by:
   * 
   * - Attaching a script tag to the header with a source pointing to the
   * JSONP formatted listing map.
   * - Creating the function the JSONP will call after it has loaded.
   * - Calling the given callback when the listing map has been received.
   * 
   * @param {string} s3ListingMapJSONPURL - URL (or path) to the S3 Listing Map JSONP.
   * @param {loadS3ListingMapCB} rootCB - Called on error or completion.
   */
  S3ListingMapLoader.load = function(s3ListingMapJSONPURL, rootCB) {
    // attach a script tag that will load the listing map JSONP
    var script = document.createElement("script");
    script.type = "application/javascript";
    script.src  = s3ListingMapJSONPURL;
    
    var head = document.getElementsByTagName("head")[0];
    head.appendChild(script);
    
    // create JSONP callback function
    window[Config.s3ListingMapJSONPFunctionName] = function(s3ListingMap) {
      return rootCB(null, s3ListingMap);
    };
  };
  
  /**
   * @callback loadS3ListingMapCB
   * @param {string}              err          - An error if one occurred.
   * @param {module:S3ListingMap} s3ListingMap - S3 Listing Map.
   */
})();