/**
 * @module Config
 * 
 * Provides configuration.
 */
(function() {
  "use strict";
  

  var s3ListingMapJSONPS3KeyUToken = "s3ListingMap";
  
  /**
   * List of configuration properties.
   * 
   * @constant
   * @type {Object}
   * @property {string} s3ListingMapJSONPS3KeyUToken  - A unique token that identifies an object as the S3 Listing
   *                                                    Map JSONP file in S3. Any S3 object with a key containing
   *                                                    this (case insensitive) will be treated as the S3 Listing Map
   *                                                    JSONP file and will not cause the S3 Listing Map to be
   *                                                    updated. Because S3 replaces certain characters, it is
   *                                                    recommended that this contains strictly only alphanumeric
   *                                                    characters. See {@link module:LambdaHooks.onPutObject}.
   * @property {string} s3ListingMapJSONPS3Key        - S3 Key to store the JSON formatted S3 Listing Map as. It MUST
   *                                                    contain the value of s3ListingMapJSONPS3KeyUToken or an
   *                                                    infinite recursion loop will occur. See
   *                                                    {@link module:LambdaHooks.onPutObject}.
   * @property {string} s3ListingMapJSONPFunctionName - Function name to use when creating S3 Listing Map JSONP.
   * 
   * @see {@link module:LambdaHooks.onPutObject}
   */
  var Config = Object.freeze({
    s3ListingMapJSONPS3KeyUToken : s3ListingMapJSONPS3KeyUToken,
    s3ListingMapJSONPS3Key       : "S3 Listing/" + s3ListingMapJSONPS3KeyUToken + ".jsonp",
    s3ListingMapJSONPFunctionName: "loadS3ListingMap",
  });
  
  if (typeof module !== "undefined") {
    module.exports = Config;
  }
  else {
    window.Config = Config;
  }
})();