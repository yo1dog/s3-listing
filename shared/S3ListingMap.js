/**
 * @module S3ListingMap
 * 
 * Provides the {@link S3ListingMap} class.
 */
(function() {
  "use strict";
  
  
  
  /**
   * @typedef S3ListingMapDirectories
   * 
   * @type {Object}
   * @property {DirectoryEntry} *filepath* - Filepath of a directory.
   */
  
  /**
   * @class
   * @classdesc An index map of directories and files in the directories.
   * 
   * @param {S3ListingMapDirectories} directories - List of directory entries.
   */
  function S3ListingMap(directories) {
    /**
     * List of directory entries.
     * @type {S3ListingMapDirectories}
     */
    this.directories = directories || {};
  }
  
  if (typeof module !== "undefined") {
    module.exports = S3ListingMap;
  }
  else {
    window.S3ListingMap = S3ListingMap;
  }
  
  
  
  /**
   * @typedef DirectoryEntryFiles
   * 
   * @type {Object}
   * @property {FileEntry} *filename* - Filename of a file.
   */
  
  /**
   * @class
   * @classdesc A directory entry in an S3 Listing Map.
   * 
   * @param {DirectoryEntryFiles} files - List of file entries.
   */
  function DirectoryEntry(files) {
    /**
     * List of file entries.
     * @type {DirectoryEntryFiles}
     */
    this.files = files || {};
  }
  S3ListingMap.DirectoryEntry = DirectoryEntry;
  
  
  
  /**
   * @class
   * @classdesc A file entry in an S3 Listing Map.
   * 
   * @param {boolean} isDir                     - If this file is a directory (true) or a normal file (false).
   * @param {number}  [lastModifiedTimestampMS] - Time this file was last modified as a unix timestamp in miliseconds.
   * @param {number}  [sizeInBytes]             - The size of the file expressed in bytes.
   */
  function FileEntry(isDir, lastModifiedTimestampMS, sizeInBytes) {
    this.isDir                   = isDir;
    this.lastModifiedTimestampMS = typeof lastModifiedTimestampMS === "number"? lastModifiedTimestampMS : undefined;
    this.sizeInBytes             = typeof sizeInBytes             === "number"? sizeInBytes             : undefined;
  }
  S3ListingMap.FileEntry = FileEntry;
  
  
  
  /**
   * Returns this S3 Listing Map in JSONP format. The given function name will be
   * called with this as the first and only parameter.
   * 
   * @param {string} functionName - Name of the function to use in the JSONP.
   * 
   * @returns {string} JSONP formated S3 Listing Map directories.
   */
  S3ListingMap.prototype.toJSONP = function(functionName) {
    return functionName + "(" + JSON.stringify(this) + ");";
  };
})();