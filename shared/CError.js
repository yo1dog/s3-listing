/**
 * @module CError
 * 
 * Provides the {@link CError} class.
 */
(function() {
  "use strict";
  
  var LogUtil;
  
  if (typeof require !== "undefined") {
    LogUtil = require("./LogUtil");
  }
  else {
    LogUtil = window.LogUtil;
  }
  
  
  
  /**
   * @class
   * @classdesc An error with a cause.
   * 
   * @param {Error|string} error                   - The error that has occurred. If a string is given it will
   *                                                 be converted to an Error instance unless keepErrorString is
   *                                                 specified.
   * @param {Error}        cause                   - The error that caused the error that occurred.
   * @param {boolean}      [keepErrorString=false] - If error should be left as a string not not converted to
   *                                                 an Error instance.
   */
  function CError(error, cause, keepErrorString) {
    Error.captureStackTrace(this, CError);
    this.name = "CError";
    
    // set the error
    if (typeof error === "string" && !keepErrorString) {
      // if the error is a string, wrap it in an error
      this.error = new Error(error);
      Error.captureStackTrace(this.error, CError); // start the stack trace outside this class
    }
    else {
      this.error = error;
    }
    
    
    // set the cause
    this.cause = cause;
    
    
    // set the root cause
    this.rootCause = null;
    if (this.cause) {
      this.rootCause = this.cause.rootCause || this.cause; 
    }
    
    
    // set the message
    this.message = null;
    
    // use the error's message
    if (this.error) {
      this.message = this.error.message;
    }
    if (!this.message) {
      this.message = LogUtil.getInfoString(this.error);
    }
    
    // add the root cause
    if (this.rootCause) {
      this.message += " - Root Cause - " + (this.rootCause.message || LogUtil.getInfoString(this.rootCause));
    }
    
    
    // set the stack
    this.stack = "";
    
    // show the error
    // if the error is a CError, we have two chains going
    if (this.error instanceof CError) {
      // show the first CError
      this.stack += this.error.toString();
      
      // put a divider between the two CErrors so it is clear they are two different chains
      if (this.cause) {
        this.stack += "\n\n=====================\n=====================";
      }
    }
    else {
      // convert the error to a string
      var errorStr;
      
      // use the error's stack
      if (this.error) {
        errorStr = this.error.stack;
      }
      if (!errorStr) {
        errorStr = LogUtil.getInfoString(this.error);
      }
      
      this.stack += errorStr;
    }
    
    // show the cause
    this.stack += "\n\n----- Caused By -----\n";
    
    // convert cause to string
    var causeStr;
    
    // use the cause's stack
    if (this.cause) {
      causeStr = this.cause.stack;
    }
    if (!causeStr) {
      causeStr = LogUtil.getInfoString(this.cause);
    }
    
    this.stack += causeStr;
  }
  CError.prototype = new Error();
  CError.constructor = CError;
  
  
  if (typeof module !== "undefined") {
    module.exports = CError;
  }
  else {
    window.Config = CError;
  }
  
  
  
  /**
   * 
   */
  CError.prototype.toString = function() {
    return this.stack;
  };
  
  
  
  /**
   * Returns the instance of the first error of the given class in the caused-by chain
   * of the given CError.
   * 
   * @param {CError} cErr    - CError to look through the chain of.
   * @param {function} klass - Constructor (class) of the error you are looking for.
   * 
   * @returns The first instance of the given class or null.
   */
  CError.getFristInstanceOfClassFromChain = function(cErr, klass) {
    var err = cErr;
    while (err) {
      if (err instanceof klass) {
        return err;
      }
      
      err = err.cause;
    }
    
    return null;
  };
  
  
  
  /**
   * Calls toString on the error if the given error is a CError. Otherwise, it
   * simply returns the error.
   * 
   * @param {*} err - Error you are going to print.
   * 
   * @returns {*}
   */
  CError.toPrint = function(err) {
    if (err instanceof CError) {
      return err.toString();
    }
    else {
      return err;
    }
  };
})();