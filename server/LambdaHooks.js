/**
 * @module LambdaHooks
 * 
 * Provides hooks for AWS Lambda to call on event triggers.
 */
"use strict";

var Config                   = require("../shared/Config");
var S3ListingMapJSONPUpdater = require("./S3ListingMapJSONPUpdater");
var CError                   = require("../shared/CError");



/**
 * Hook for AWS Lambda to call when the "Put Object" event is triggered.
 * 
 * Updates the S3 Listing Map for each bucket in the event records.
 * 
 * @param {Object} event   - Event object from Lambda.
 * @param {Object} context - Context object from Lambda.
 */
function onPutObject(event, context) {
  var bucketsUpdated = [];
  
  // process each event record
  asyncForEach(event.Records, 0, function(record, asyncCB) {
    var bucket = record.s3.bucket.name;
    var key    = record.s3.object.key;
    
    console.log("Bucket: " + bucket);
    console.log("Key   : " + key);
    
    // Don't do anything if the object being updated is our listing map, otherwise
    // we would get caught in an infinite recursion loop. eg. Put object hook is
    // triggered, update S3 Listing Map and put S3 Listing Map JSONP, Put object
    // hook is triggered, etc.
    //
    // We use this unique token method because it is safer than comparing the keys
    // directly as the S3 key from the record will not always match the S3 key of
    // the object created. For example, S3 replaces all spaces in the path name with
    // '+' so if your Config.s3ListingMapJSONPS3Key contains spaces a direct
    // comparison would return false and you would get caught in a loop.
    //
    // Therefore, it is important that the key used to put the S3 Listing Map JSONP
    // (Config.s3ListingMapJSONPS3Key) contains the unique token
    // (Config.s3ListingMapJSONPS3KeyUToken), otherwise we will not be able to tell
    // it is the listing map and the infinite recursion loop will occur.
    if (key.toLowerCase().indexOf(Config.s3ListingMapJSONPS3KeyUToken.toLowerCase()) > -1) {
      console.log("Is listing map, skipping...");
      return process.nextTick(asyncCB);
    }
    
    // don't update the S3 Listing Map for the same bucket twice
    if (bucketsUpdated.indexOf(bucket) > -1) {
      console.log("Already updated this bucket, skipping...");
      return process.nextTick(asyncCB);
    }
    
    // update map
    console.log("Updating bucket...");
    S3ListingMapJSONPUpdater.updateForBucket(bucket, function(err) {
      if (err) return asyncCB(new CError("Error updating S3 Listing Map JSONP for bucket \"" + bucket + "\".", err));
      
      console.log("done!");
      bucketsUpdated.push(bucket);
      return asyncCB();
    });
  },
  function(err) {
    if (err) {
      if (err instanceof CError) {
        return context.done("error", err.message + "\n" + err.rootCause);
      }
      
      return context.done("error", err);
    }
    
    return context.done(null, "");
  });
}
module.exports.onPutObject = onPutObject;



/**
 * Executes a function for each given item in order and in series. Execution stops on
 * first error.
 * 
 * @param {*[]}      items    - List of items to iterate over.
 * @param {number}   index    - Starting position in the list of items.
 * @param {function} iterator - Function to call for each item. Should accept the item as
 *                              the first param and a callback as the second.
 * @param {asyncForEachCB} rootCB - Called on error or on completion.
 */
function asyncForEach(items, index, iterator, rootCB) {
  if (index >= items.length) {
    return process.nextTick(rootCB);
  }
  
  iterator(items[index], function(err) {
    if (err) return rootCB(err);
    
    ++index;
    return asyncForEach(items, index, iterator, rootCB);
  });
}

/**
 * @callback asyncForEachCB
 * @param {string} err - An error if one occurred.
 */