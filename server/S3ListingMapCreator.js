/**
 * @module S3ListingMapCreator
 * 
 * Provides the {@link S3ListingMapCreator} class.
 */
"use strict";

var nPath                 = require("path");
var RelativeFilepathError = require("./RelativeFilepathError");
var S3ListingMap          = require("../shared/S3ListingMap");



/**
 * @class
 * @classdesc A utility for building an {@link module:S3ListingMap}.
 * @requires module:S3ListingMap
 * @requires module:path
 * @requires module:RelativeFilepathError
 */
function S3ListingMapCreator() {
  this.map = new S3ListingMap();
}
module.exports = S3ListingMapCreator;



/**
 * @class
 * @classdesc Generic description of a file.
 * 
 * @param {string}  filepath                  - Filepath of the file. Note that capitalization is significant
 *                                            - (eg. /path/file.ext is NOT the same as /Path/file.ext).
 * @param {boolean} isDir                     - If this file is a directory (true) or a normal file (false).
 * @param {number}  [lastModifiedTimestampMS] - Time this file was last modified as a unix timestamp in milliseconds.
 * @param {number}  [sizeInBytes]             - The size of the file expressed in bytes.
 */
function FileDescriptor(filepath, isDir, lastModifiedTimestampMS, sizeInBytes) {
  this.filepath                = filepath;
  this.isDir                   = isDir;
  this.lastModifiedTimestampMS = typeof lastModifiedTimestampMS === "number"? lastModifiedTimestampMS : undefined;
  this.sizeInBytes             = typeof sizeInBytes             === "number"? sizeInBytes             : undefined;
}
S3ListingMapCreator.FileDescriptor = FileDescriptor;



/**
 * Adds a list of files to the map.
 * 
 * @param {FileDescriptor[]} fileDescriptors - Files to add to the map. Calls {@link S3ListingMapCreator#addFile}
 *                                             internally.
 * 
 * @see S3ListingMapCreator#addFile
 */
S3ListingMapCreator.prototype.addFiles = function(fileDescriptors) {
  for (var i = 0; i < fileDescriptors.length; ++i) {
    this.addFile(fileDescriptors[i]);
  }
};



/**
 * Adds a file to the map.
 * 
 * @param {FileDescriptor} fileDescriptor - File to add to the map.
 * 
 * @throws {RelativeFilepathError}
 */
S3ListingMapCreator.prototype.addFile = function(fileDescriptor) {
  // format the filepath
  var filepathData = formatFilepath(fileDescriptor.filepath);
  var filepath = filepathData.filepath;
  
  if (fileDescriptor.isDir) {
    // create directory entry if it does not exists
    if (!this.map.directories[filepath]) {
      this.map.directories[filepath] = new S3ListingMap.DirectoryEntry();
    }
  }
  
  // if we don't have a parent directory, the filepath must be "/" and
  // there is nothing left to do
  if (!filepathData.parentDirFilepath) {
    return;
  }
  
  // get parent directory entry
  var parentDirEntry = this.map.directories[filepathData.parentDirFilepath];
  
  // create parent directory entry if it does not exist
  if (!parentDirEntry) {
    try {
      this.addFile(new FileDescriptor(filepathData.parentDirFilepath, true));
    }
    catch (err) {
      if (err instanceof RelativeFilepathError) {
        // bubble up the error using this filepath so by the time it gets out of this function
        // the originally given filepath is used in the error. eg. We want the error to show
        // the filepath "/dir1/.../dir2/file.ext" instead of "/dir1/.../" which is the filepath
        // when the error is originally thrown.
        throw new RelativeFilepathError(fileDescriptor.filepath);
      }
      
      throw err;
    }
    
    parentDirEntry = this.map.directories[filepathData.parentDirFilepath];
  }
  
  // add filepath to parent directory entry
  // if a file entry already exists, combine the data
  var oldFileEntry = parentDirEntry.files[filepathData.filename] || {};
  
  parentDirEntry.files[filepathData.filename] = new S3ListingMap.FileEntry(
    fileDescriptor.isDir,
    fileDescriptor.lastModifiedTimestampMS || oldFileEntry.lastModifiedTimestampMS,
    fileDescriptor.sizeInBytes             || oldFileEntry.sizeInBytes
  );
};



/**
 * Returns the S3 Listing Map we have created.
 * 
 * @returns {module:S3ListingMap}
 */
S3ListingMapCreator.prototype.getS3ListingMap = function() {
  return this.map;
};



/**
 * Formats a filepath for use in the map and other useful info about the filepath.
 * 
 * - Win32 and posix-style file separators are supported.
 * - Filepaths ending with a file separator ('/' or '\') are considered directories.
 * - Filepaths will be converted to use posix-style file separators.
 * - Filepaths will be made absolute  if they are not already by appending a '/'.
 * - Directory relatives ("." and "..") are NOT supported except for a current directory
 *   relative (".") at the very beginning of the path (eg. ./path/file.ext) which will
 *   be removed to make the filepath absolute.
 * 
 * @param {string} filepath - Filepath to format.
 * 
 * @returns {Object}      filepathData                   Info about the given filepath.
 * @returns {string}      filepathData.filepath          The formated filepath.
 * @returns {boolean}     filepathData.isDir             If the given filepath is a directory.
 * @returns {string}      filepathData.filename          The filename of the given filepath.
 * @returns {string|null} filepathData.parentDirFilepath The filepath of the parent directory of the given filepath.
 *                                                       Will be null if the formated filepath is "/" (eg. if you
 *                                                       pass in "", "/", ".", etc.)
 * 
 * @throws {RelativeFilepathError}
 */
function formatFilepath(rawFilepath) {
  var filepath = rawFilepath;
  
  // convert win32 file separators ('\') to posix ('/')
  filepath = filepath.replace(/\\/g, "/");
  
  // remove all double slashes
  filepath = filepath.replace(/\/\/+/g, "/");
  
  // convert to an absolute path
  if (filepath.length === 0 || filepath.charAt(0) !== "/") {
    filepath = "/" + filepath;
  }
  
  // remove beginning current-directory-relative (.)
  if (filepath === "/.") {
    filepath = "/";
  }
  else if (filepath.indexOf("/./") === 0) {
    filepath = filepath.substring(2);
  }
  
  
  // get the filename
  var filename = nPath.basename(filepath);
  
  // check for unsupported directory relatives
  if (filename === "." || filename === "..") {
    throw new RelativeFilepathError(rawFilepath);
  }
  
  
  // get the parent directory filepath
  var parentDirFilepath;
  
  if (filepath === "/") {
    parentDirFilepath = null;
  }
  else {
    parentDirFilepath = nPath.dirname(filepath);
    
    if (parentDirFilepath.length === 0 || parentDirFilepath.charAt(parentDirFilepath.length - 1) !== "/") {
      parentDirFilepath = parentDirFilepath + "/";
    }
  }
  
  
  // done!
  return {
    filepath         : filepath,
    filename         : filename,
    parentDirFilepath: parentDirFilepath
  };
}