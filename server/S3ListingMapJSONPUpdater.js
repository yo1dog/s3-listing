/**
 * @module S3ListingMapJSONPUpdater
 * 
 * Provides functions for updating the S3 Listing Map JSONP for an S3 bucket.
 */
"use strict";

var Config              = require("../shared/Config");
var S3Helper            = require("./S3Helper");
var S3ListingMapCreator = require("./S3ListingMapCreator");
var CError              = require("../shared/CError");



/**
 * Creates a JSONP formatted S3 Listing Map for the given S3 bucket and saves it to S3.
 * 
 * The listing map is saved in the same bucket it is for and as the key given
 * by {@link module:Config#s3ListingMapJSONPS3Key}.
 * 
 * @param {string} bucket - Bucket to generate the map for.
 * @param {updateForBucketCB} rootCB - Called on error or completion.
 */
function updateForBucket(bucket, rootCB) {
  // get all files in the bucket
  console.log("Getting filepaths in bucket...");
  S3Helper.getFilepathsInBucket(bucket, function(err, s3Files) {
    if (err) return rootCB(new CError("Error getting filepaths from bucket \"" + bucket + "\".", err));
    
    // create file descriptors for each file
    var fileDescriptors = [];
    for (var i = 0; i < s3Files.length; ++i) {
      var s3File = s3Files[i];
      
      var filepath    = s3File.Key;
      var isDir       = filepath.charAt(filepath.length - 1) === "/";
      var sizeInBytes = s3File.Size;
      
      var lastModifiedTimestampMS;
      try {
        lastModifiedTimestampMS = Date.parse(s3File.LastModified);
      }
      catch(err) {
        lastModifiedTimestampMS = null;
      }
      
      fileDescriptors[i] = new S3ListingMapCreator.FileDescriptor(filepath, isDir, lastModifiedTimestampMS, sizeInBytes);
    }
    
    // create an S3 Listing Map
    console.log("Creating map...");
    var s3ListingMap;
    try {
      var s3ListingMapCreator = new S3ListingMapCreator();
      s3ListingMapCreator.addFiles(fileDescriptors);
      
      s3ListingMap = s3ListingMapCreator.getS3ListingMap();
    }
    catch (err) {
      return rootCB(new CError("Error creating S3 Listing Map for bucket \"" + bucket + "\".", err));
    }
    
    
    // format listing map as JSONP
    var s3ListingMapJSONP = s3ListingMap.toJSONP(Config.s3ListingMapJSONPFunctionName);
    
    // save listing map
    console.log("Saving map...");
    S3Helper.saveS3ListingMap(bucket, Config.s3ListingMapJSONPS3Key, s3ListingMapJSONP, function(err) {
      if (err) return rootCB(new CError("Error saving S3 Listing Map for bucket \"" + bucket + "\".", err));
      
      return rootCB();
    });
  });
}

module.exports.updateForBucket = updateForBucket;

/**
 * @callback updateForBucketCB
 * @param {string} err - An error if one occurred.
 */