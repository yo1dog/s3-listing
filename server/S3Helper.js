/**
 * @module S3Helper
 * 
 * Provides functions for interfacing with AWS S3.
 */
"use strict";

var CError = require("../shared/CError");

var AWS = require("aws-sdk");
var S3  = new AWS.S3();



/**
 * Returns a list of filepaths for all files in the given S3 bucket.
 * 
 * @param {string} bucket - Bucket to search.
 * @param {getFilepathsInBucketCB} rootCB - Called on error or completion.
 * 
 * @see module:aws-sdk.S3#listObjects
 */
function getFilepathsInBucket(bucket, rootCB) {
  // list objects
  var params = {
    Bucket: bucket
  };
  
  S3.listObjects(params, function(err, data) {
    if (err) return rootCB(new CError("Error getting objects from S3 bucket \"" + bucket + "\".", err));
    
    var filepaths = data.Contents;
    return rootCB(null, filepaths);
  });
}
module.exports.getFilepathsInBucket = getFilepathsInBucket;

/**
 * @callback getFilepathsInBucketCB
 * @param {string}   err       - An error if one occurred.
 * @param {string[]} filepaths - List of filepaths in the bucket.
 */



/**
 * Saves the given content in the given S3 bucket.
 * 
 * @param {string} bucket            - Bucket to save the listing map in.
 * @param {string} key               - Key to save the listing map as.
 * @param {string} s3ListingMapJSONP - Content to save.
 * @param {getFilepathsInBucketCB} rootCB - Called on error or completion.
 * 
 * @see module:aws-sdk.S3#putObject
 */
function saveS3ListingMap(bucket, key, s3ListingMapJSONP, rootCB) {
  // put object
  var params = {
    Bucket  : bucket,
    Key     : key,
    Body    : s3ListingMapJSONP
  };
  
  S3.putObject(params, function(err, data) {
    if (err) {
      return rootCB(new CError("Error putting object to S3 bucket \"" + bucket + "\" as key \"" + key + "\".", err));
    }
    
    return rootCB();
  });
}
module.exports.saveS3ListingMap = saveS3ListingMap;

/**
 * @callback saveS3ListingMapCB
 * @param {string} err - An error if one occurred.
 */