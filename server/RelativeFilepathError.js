/**
 * @module RelativeFilepathError
 * 
 * Provides the {@link RelativeFilepathError} class.
 */
"use strict";

/**
 * @class
 * @implements {Error}
 * @classdesc An error that describes using directory relatives when they are prohibited.
 * 
 * @param {string} filepath - The filepath that contains directory relatives.
 */
function RelativeFilepathError(filepath) {
  Error.captureStackTrace(this, RelativeFilepathError);
  this.name = "RelativeFilepathError";
  
  this.filepath = filepath;
  this.message  = "Directory relatives (\".\" and \"..\") used in filepath \"" + filepath + "\". Directory relatives are not supported.";
}
RelativeFilepathError.prototype = new Error();
RelativeFilepathError.constructor = RelativeFilepathError;

module.exports = RelativeFilepathError;